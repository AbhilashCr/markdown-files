# Search Engines 
Searches are integral parts of any application. Performing searches on terabytes and petabytes
 of data can be challenging when speed, performance, and high availability are core requirements. This paper will comparsion beetween three such search engines. 

 --- 
## Lucene

[Lucene](https://lucene.apache.org/core/documentation.html) is a full-text search library in Java which makes it easy to add search functionality to an application or website.
The content you add to Lucene can be from various sources, like a SQL/NoSQL database, a filesystem, or even from websites.

Lucene library provides the core operations which are required by any search application. Elasticsearch and Solr are complete application built on top lucene.

**Features**
1. Scalable, High-Performance Indexing.
2. Small RAM Requirements – Only 1 MB Heap.
3. Incremental Indexing.
4. Fielded Searching.

**Simple Lucene query**

 Below query search for phrase "foo bar" in the title field AND the phrase "quick fox" in the body field.

 ```
title:"foo bar" AND body:"quick fox"
 ```

 ---
## Elasticsearch 

[Elasticsearch](https://www.elastic.co/guide/index.html) is a highly scalable open-source full-text search and analytics engine built on top of Apache Lucene. It allows you to store, search, and analyze big volumes of data quickly and in near real time.



**Elasticsearch is standing as a NOSQL DB because:**

1. It easy-to-use
2. Has a great community
3. Complatibility with JSON
4. Broad use cases

**Features**

1. Real-Time Analysis
2. Broad Distribution
3. Reliability
4. Open Source License

**Simple Elasticsearch query**

```
curl -XGET 'localhost:9200/app/users/4?pretty'
```

Responce

```{
  "_index" : "app",
  "_type" : "users",
  "_id" : "4",
  "_version" : 1,
  "found" : true,
  "_source" : {
    "id" : 4,
    "username" : "john",
    "last_login" : "2018-01-25 12:34:56"
  }
}
```
---

## Apache solr

[Apache Solr](https://lucene.apache.org/solr/guide/)is an Open-source REST-API based search server platform written in java language by apache software foundation.
Solr is highly scalable, ready to deploy, search engine that can handle large volumes of text-centric data and it is built on top of Apache Lucene.

Solr offers powerful features such as distributed full-text search, faceting, near real-time indexing, high availability, NoSQL features, integrations with big data tools such as Hadoop, and the ability to handle rich-text documents such as Word and PDF.

**Features of Apache Solr**

1. Automatic Load Balancing
2. Standards-Based Open Interfaces – XML, JSON, and HTTP
3. Autocomplete/Type-ahead Prediction
4. Faceted Search and Filtering 
5. Central Configuration For Entire Cluster

**Simple Solr query** 

```
http://localhost:8983/solr/techproducts/select?q=sd500&wt=json
```
Responce

```
   {
    "responseHeader": {
        "status": 0,
        "QTime": 3,
        "params": {
            "q": "sd500",
            "wt": "json"
        }
    }
```

---
 ## Comparsion between solr and Elasticsearch
Selectind between these two technologies requires a complete understanding of the use cases they support, their feature sets, the scaling option.

### Data Sources
Both tools support a wide range of data sources.

Solr uses request handlers to ingest data from XML files, CSV files, databases, Microsoft Word documents, and PDFs. With native support for the Apache Tika library, it supports extraction and indexing from over one thousand file types.

Elasticsearch, on the other hand, is completely JSON-based. It supports data ingestion from multiple sources using the Beats family (lightweight data shippers available in the Elastic Stack) and Logstash.

### Searching 
 Both Solr and Elastic search support near realtime searches and includes all Lucene's search capabilities and some of there own features. both support JSON-based Query DSL.

 In Solr we can write very complex search queries and it includes a UI called Velocity Search that offers powerful features such as searching, highlighting, autocomplete etc.

 ### Indexing
 Both search engines supports schemaless indexing, custom analysers, Synonym-based indexing.

 It is easy to index unstructured data and dynamic fields without defining the schema.

 ### Scalability and Clustering
Search engines have to quickly process large amounts of data and complex queries on sets of hundreds of millions of records. Sometimes these queries can be so resource-intensive that they can take the whole system down. For this this reason search engine must scalable.

Elasticsearch’s design has horizontal scaling in mind, it has better support for scaling and cluster management. but once created shards cannot be increased. Elasticsearch uses shrink API to reduce the shards of an index. Slor supports splitting of an existing shard but not the shrinking of shards.

### Community
Solr had a broad, open source community. Anyone can still contribute to Solr.

Elasticsearch is technically open source but not fully. All contributors have access to the source code, and users can make changes and contribute them. But final changes get confirmation from cpmoany itself.

### Documentation
Elasticsearch’s official website offer well-organized, high quality documentation with clear examples.

Solr was a very well-documented product with clear examples and contexts for API use cases. However, its documentation maintenance has fallen behind.

---
## Conclusion

Both of these technologies are quite easy to begin working with. Solr offers great functionalities in the field of information retrieval, but Elasticsearch is much easier to take into production and scale. 

---
## References
1.[Elastic search Documentation](https://www.elastic.co/webinars/getting-started-elasticsearch-v2?baymax=rtp&elektra=home&storm=banner)

2.[Solr documentation](https://lucene.apache.org/solr/guide/8_6/solr-tutorial.html)

3.[Apache Lucene](https://lucene.apache.org/)

4.[Solr vs Elasticsearch](https://logz.io/blog/solr-vs-elasticsearch/)


